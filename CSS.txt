@charset "utf-8";
/* CSS Document */

body
{
	background-color:#930;
	margin:0 auto;

}

.banner
{
	height:150px;
	background-color:#CCC;
	border-style:inset;
	text-align:center;
	font-family:"Courier New", Courier, monospace;
	font-size:large;
	color:#00F;
}

.menu
{
	height:100px;
	background-color:#069;
	border-style:outset;
	text-align:center;
	font-family:"Courier New", Courier, monospace;
	font-size:18px;
	color:#000;
	border-radius:5px;
	box-shadow:red;
	

}

.layer
{
	height:700px;
	background-color:#990;
	border-style:groove;
	text-align:center;
	font-family:"Courier New", Courier, monospace;
	font-size:18px;
	color:#000;
}

.footer
{
	height:90px;
	background-color:#F0F;
	border-style:ridge;
	text-align:center;
	font-family:"Courier New", Courier, monospace;
	font-size:18px;
	color:#000;
}

.header
{
	width:10000px;
	height:100px;
	padding-top:20px;
}

.buttons
{
	text-align:center;
	height:56px;
	padding-left:0px;
	padding:0px 0px 0px 100px;
}

.buttons a
{
	font-family:Georgia, "Times New Roman", Times, serif;
	font-size:18px;
	font-weight:100;
	display:block;
	float:left;
	text-decoration:none;
	color:#903;
	text-align:center;
	padding-top:22px;
	height:34px;
	text-shadow:#333 2px 2px 4px;
}

.but
{
	width:160px;
}

.button.but:hover
{
	color:#0F6;
	text-shadow:#333 1px 1px 1px;
}